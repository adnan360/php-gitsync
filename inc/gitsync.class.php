<?php
require dirname(dirname(__FILE__)) . '/vendor/autoload.php';

/**
 * Class that checks and syncs with git repos without system git command.
 *
 * Excellent for shared host environments without SSH feature or git installed.
 */
class GitSync {
	public $config_file;
	public $config;

	/**
	 * Construct for the class to set the config etc.
	 *
	 * @param $config_input (array|string) Either the config file or the config
	 *   array being passed.
	 */
	function __construct($config_input) {
		if (is_array($config_input)) {
			$this->config = $config_input;
		} else {
			$this->config_file = $config_input;
			require_once($this->config_file);
			$this->config = $config;
		}

		// Default values
		if (!isset($this->config['cache_dir'])) {
			$this->config['cache_dir'] = '.cache';
		}

		// Required php file
		require_once( dirname(__FILE__) . '/ziparchive_extend.class.php' );
		require_once( dirname(__FILE__) . '/UnifiedDiffPatcher.php' );
	}

	function msg($message) {
		echo "<div>## $message</div>";
	}

	function get_cache_filename($repo_url) {
		return $this->config['cache_dir'].'/'.md5($repo_url);
	}

	function get_last_cache_filename($repo_url) {
		if (file_exists($this->get_cache_filename($repo_url))) {
			return $this->get_cache_filename($repo_url);
		} else {
			return false;
		}
	}

	function get_latest_changes($repo_config) {
		$latest_data = array();
		$host = $this->get_repo_host_type($repo_config);
		// -- GitLab
		if ($host == 'gitlab') {
			$checkurl = $repo_config['repo_url'].'/-/commits/master';
			$document = phpQuery::newDocumentHTML(file_get_contents($checkurl));
			$latest_data['commit_message'] = pq('.commit-row-message.item-title:first')->text();
			$latest_data['commit_id'] = trim(pq('.commit-sha-group:first .label')->text());
			return $latest_data;
		// -- GitHub
		} elseif ($host == 'github') {
			$checkurl = $repo_config['repo_url'].'/commits/master';
			$document = phpQuery::newDocumentHTML(file_get_contents($checkurl));
			$latest_data['commit_message'] = pq('.commit-title:first')->text();
			$latest_data['commit_id'] = trim(pq('.commit-links-group:first a.sha.btn')->text());
			return $latest_data;
		} else {
			$this->msg('ERROR: Does not support syncing from this website... '.$repo_config['repo_url']);
			return false;
		}
	}

	function update_cache($repo_url, $cache_data) {
		$cache_filename = $this->get_cache_filename($repo_url);
		return file_put_contents($cache_filename, $cache_data);
	}

	/**
	 * Returns a lowercase slug of the provider.
	 *
	 * This usually depends on the HTML structure of the site. e.g. All the
	 * GitLab instances have the same HTML structure, so it should return
	 * `gitlab`. For non-standard domains running GitLab, set the
	 * `repo_host_type` value in config.
	 *
	 * @param $repo Can take either repo config array or repo url.
	 *     Repo config array is recommended.
	 */
	function get_repo_host_type($repo) {
		if (is_array($repo)) {
			if (isset($repo['repo_host_type']) && $repo['repo_host_type'] == 'gitlab') {
				return 'gitlab';
			}
			$repo_url = $repo['repo_url'];
		} else {
			$repo_url = $repo;
		}
		if (substr($repo_url, 0, 19) == 'https://gitlab.com/') {
			return 'gitlab';
		} elseif (substr($repo_url, 0, 19) == 'https://github.com/') {
			return 'github';
		}
	}

	function get_download_url_for_repo($repo_config) {
		$ret = array();
		$host = $this->get_repo_host_type($repo_config);
		if ($host == 'gitlab') {
			$document = phpQuery::newDocumentHTML(file_get_contents($repo_config['repo_url']));
			$ret['url'] = 'https://gitlab.com' . pq('.project-action-button .btn-group a:first')->attr('href');
			$ret['save'] = $this->config['cache_dir'].'/'.md5($repo_config['repo_url']).'.zip';
			$ret['basedir'] = basename($repo_config['repo_url']).'-master/';
		} elseif ($host == 'github') {
			$ret['url'] = $repo_config['repo_url'].'/archive/master.zip';
			$ret['save'] = $this->config['cache_dir'].'/'.md5($repo_config['repo_url']).'.zip';
			$ret['basedir'] = basename($repo_config['repo_url']).'-master/';
		}
		return $ret;
	}

	function update_repo($repo_config) {
		// Download the repo archive into cache directory
		$download = $this->get_download_url_for_repo($repo_config);
		copy($download['url'], $download['save']);

		// Extract to destination directory
		@mkdir($repo_config['dest_path'], 0755, true);
		$zip = new ZipArchive_extend;
		if ($zip->open($download['save']) === TRUE) {
			//$zip->extractTo($repo_config['dest_path']);
			$zip->extractSubdirTo($repo_config['dest_path'], $download['basedir']);
			$zip->close();
			$this->msg('Extracted '.$download['save'].' to '.$repo_config['dest_path']);
			@unlink($download['save']);
		} else {
			$this->msg('Failed to extract '.$download['save']);
		}
	}

	function patch_files($patch_file, $files_dir, $patch_level=1) {
		$patch_file_dest = realpath($files_dir).'/'.basename($patch_file);
		copy($patch_file, $patch_file_dest);

		// Run the Patching process
		try{
			$diff = new UnifiedDiffPatcher();
			$patchPath = $patch_file_dest; //Absolute path of the patch file.
			//chdir('../');//The folder from where the patch path are relative to.

			$ret = $diff->validatePatch($patchPath, $patch_level, true); //Validate the patch, and display debug informations
			if ($ret !== false) {
				$diff->processPatch($patchPath, $patch_level); //Apply the patch without displaying debug informations.
			} else {
				echo 'Error repport:' . "\n";
				echo implode("\n", $diff->getError());
			}
		}
		catch(Exception $e) { //This will catch critical error witch can't be recovered.
							  //Like file access errors.
			echo "\n" . $e->getMessage();
		}
		unlink($patch_file_dest);
	}

	function do_sync() {
		$ret = array(
			'global' => array( // Applies to all repos
				'change_detected' => false,
				'updated_cache'   => false,
				'updated_output'  => false
			)
		);
		foreach ($this->config['repos'] as $key => $repo) {
			$cache_filename = $this->get_last_cache_filename($repo['repo_url']);
			$changes = json_encode($this->get_latest_changes($repo));
			if (file_exists($cache_filename)) {
				$changes_cache = file_get_contents($cache_filename);
				if ($changes == $changes_cache) {
					$update_cache=false;
					$this->msg('No new commits found. Skipping download of '.$repo['repo_url']);
				} else {
					$update_cache=true;
					$ret['global']['change_detected'] = true;
					$this->msg('New commits found. Downloading latest '.$repo['repo_url']);
				}
			} else {
				$update_cache=true;
			}

			if ($update_cache) {
				$this->update_cache($repo['repo_url'], $changes);
				$ret['global']['updated_cache'] = true;
				$this->msg('Updated cache '.$repo['repo_url']);
				$this->update_repo($repo);
				$ret['global']['updated_output'] = true;
				$this->msg('Successfully updated from repo '.$repo['repo_url']);

				// Apply Patch (if any)
				if (isset($repo['patch'])) {
					if (isset($repo['patch']['patch_file'])) { // single patch file
						$this->patch_files($repo['patch']['patch_file'], $repo['dest_path'], $repo['patch']['patch_level']);
					} else { // multiple patch files
						foreach ($repo['patch'] as $patch_key => $patch) {
							$this->patch_files($patch['patch_file'], $repo['dest_path'], $patch['patch_level']);
						}
					}
				}
			}
		}
		return $ret;
	}
}
