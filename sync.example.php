<?php
// Example minimal sync file

require_once('inc/gitsync.class.php');

$gitsync = new GitSync('config/gitsync.config.inc.php');
$gitsync->do_sync();

/*
This is also possible:
$ret = $gitsync->do_sync();
if ($ret['global']['updated_output'] == true) {
	// do something if files are updated to a new commit
}
*/
