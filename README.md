# PHP GitSync

_Sync any git repository on your shared hosts (even where there is no SSH or git)._

- Works with GitLab and GitHub projects.
- Supports multiple repositories from a single installation.
- Supports applying patch files to apply changes automatically.
- Should be easy to run through `cron` and do auto sync.
- Fairly lightweight and does the job.

Should be awesome if you have a static site on a git repo and want to mirror it. Or any other software for that matter.


### Limitations

- Currently supports only public projects but post an issue if you're interested in private projects.
- Currently only supports grabbing `master` branch.


### How it works

Easy! It does the following:

- Gets the repo's latest commit list and gets the first commit message
- Checks if we've previously updated this commit into our output folder
	- If so, skips
	- If not,
		- downloads the repo `master` zip file into the cache folder
		- extracts to destination directory without the `reponame-master` directory
		- deletes the downloaded zip file after extraction


## Install

- Upload this repo to your server somewhere.
- Copy `config/gitsync.config.inc.php.example` to `config/gitsync.config.inc.php`
- Edit as desired
- Create the cache directory you specified and the output directory
- Run `sync.example.php` in a browser and let it start the magic!


## Update

To update required libraries:
```
composer update
composer dumpautoload -o
```


## Examples

- `sync.example.php` shows basic usage with a config file. Requires a config file to be there.
- `sync.example2.php` shows running the sync with an array as config, without a dedicated config file. Great for experts!
- To use diff, add this:
```
array(
		...,
		'patch' => array(
				'patch_file' => 'config/changes.patch',
				'patch_level' => 1,
			)
	),
```

Multiple patches are also possible:

```
array(
		...,
		'patch' => array(
				array(
					'patch_file' => 'config/changes1.patch',
					'patch_level' => 1,
				),
				array(
					'patch_file' => 'config/changes2.patch',
					'patch_level' => 1,
				)
			)
	),
```

**Note:** If you have `\ No newline at end of file` in your patch file, delete those lines because it will raise an error. This is because [UnifiedDiffPatcher](https://github.com/idealtech/UnifiedDiffPatcher/) does not support this.


### Sample Output

```
## Updated cache https://gitlab.com/mjimad/projet01
## Extracted .cache/eef45e1ff6c0fbf50191cd5eea1a3d6d.zip to output/gitlabtest
## Successfully updated from repo https://gitlab.com/mjimad/projet01
## Updated cache https://github.com/diogob/git-tutorial
## Extracted .cache/cd3da581db85b728ab13cbf4b0ff4b93.zip to output/githubtest
## Successfully updated from repo https://github.com/diogob/git-tutorial
```


## Security considerations

- Delete or rename `sync.example.php` and create your own copy of this file with `$_GET` request checks
- It should be possible to put some of the files outside the `www` or `public_html` directory which would make it harder for intruders to try something


## Troubleshooting

- **I want to force update the output even if there are no new commits**: Delete all files inside your cache folder (default: `.cache`), except `.htaccess` and run your script. As there are no memories of the last commit, it will download all the repos regardless of changes.


License: MIT (Expat) License
