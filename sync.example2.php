<?php
// Example minimal sync file

require_once('inc/gitsync.class.php');

$gitsync = new GitSync(array(
	'cache_dir' => '.cache',
	'repos'=> array(
		array(
			// without leading slash (/)s
			'repo_url' => 'https://gitlab.com/mjimad/projet01',
			// in case it is a `gitlab` host, but not gitlab.com
			//'repo_host_type' => 'gitlab',
			'dest_path' => 'output/gitlabtest'
		),

		array(
			// without leading slash (/)s
			'repo_url' => 'https://github.com/phpquery/phpquery',
			// in case it is a `gitlab` host, but not gitlab.com
			//'repo_host_type' => 'gitlab',
			'dest_path' => 'output/githubtest'
		),

		array(
			// without eading slash (/)s
			'repo_url' => 'https://gitlab.com/DevAlone/proxy_py',
			// in case it is a `gitlab` host, but not gitlab.com
			//'repo_host_type' => 'gitlab',
			'dest_path' => 'output/test3'
		)
	)
));
$gitsync->do_sync();
